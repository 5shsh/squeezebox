# squeezebox

Docker image providing a squeezebox MQTT bridge.
Based on https://github.com/binsoul/net-mqtt-client-react .

## Run

```
docker run -d \
    --name squeezebox \
    --restart=unless-stopped \
    --env-file /config/env \
    homesmarthome/squeezebox:0.1.0
```
Expects ```MQTT_HOST``` and ```LOGITECH_SERVER_HOST```value in env file.