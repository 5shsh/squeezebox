<?php

define('TOPIC_PREFIX', '/squeezebox');
$MQTT_HOST = getenv('MQTT_HOST');
$LOGITECH_SERVER_HOST = getenv('LOGITECH_SERVER_HOST');

function control($host, $params) {
    $url = "http://$host:9000/jsonrpc.js";

    print_r($params);

    $payload = [
        "id" => 1,
        "method" => "slim.request",
        "params" => [
            "00:04:20:2b:c2:e7",
            $params //["power","0"]
        ]
    ];
    
    $payload = json_encode($payload);

    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    curl_close($ch);
}

$c = new Mosquitto\Client;
$c->onConnect(function() use ($c) {
    $c->subscribe(TOPIC_PREFIX . '/#', 2);
});

$c->onMessage(function($message) use ($LOGITECH_SERVER_HOST) {
    $topic = str_replace(TOPIC_PREFIX, '', $message->topic);
    printf("Got a message on topic %s with payload:\n%s\n", $message->topic, $message->payload);
    $payload = json_decode($message->payload, true);
    print_r($payload);
    switch ($topic) {
        case '/power':
            if ($payload['val'] == 1) {
                control($LOGITECH_SERVER_HOST, ["play"]);
            }
            elseif ($payload['val'] == 0) {
                control($LOGITECH_SERVER_HOST, ["power","0"]);
            }
            break;

        case '/vol':
            if (isset($payload['val'])) {
                $val = (string)$payload['val'];
                control($LOGITECH_SERVER_HOST, ["mixer","volume", $val]);
            }
            break;

        case '/vol/up':
            if (isset($payload['val'])) {
                $val = "+" . (string)$payload['val'];
                control($LOGITECH_SERVER_HOST, ["mixer","volume", $val]);
            }
            else {
                control($LOGITECH_SERVER_HOST, ["mixer","volume", "+2"]);
            }
            break;

        case '/vol/down':
            if (isset($payload['val'])) {
                $val = "-" . (string)$payload['val'];
                control($LOGITECH_SERVER_HOST, ["mixer","volume", $val]);
            }
            else {
                control($LOGITECH_SERVER_HOST, ["mixer","volume", "-2"]);
            }
            break;

        case '/play':
            control($LOGITECH_SERVER_HOST, ["playlist","play", $payload['url']]);
            break;
    }
});
echo "Connecting to $MQTT_HOST\n";
$c->connect($MQTT_HOST);
$c->loopForever();
echo "Finished\n";